package com.nbody.core.game;

import com.nbody.core.game.Body.StarType;
import com.nbody.core.game.NBodyGame.CollisionPolicy;
import com.nbody.core.game.NBodyGame.Event;
import com.nbody.core.util.Constants;
import com.nbody.core.util.FixedSizeArray;

/**
 * Force calculator for the direct particle-particle method.
 * 
 * @author Toni Sagrista
 * 
 */
public class ForceDirect extends IForceCalculator {

	public ForceDirect() {
		super();
	}

	@Override
	public void calculateForces(FixedSizeArray<Body> bodies) {
		Body bi, bj;
		for (int i = 0; i < bodies.mCount - 1; i++) {
			bi = bodies.get(i);
			for (int j = i + 1; j < bodies.mCount; j++) {
				bj = bodies.get(j);

				if (bi != null && bj != null) {
					// Create the vector from particle i to particle j
					NBodyGame.fvec.set(bj.pos.x - bi.pos.x, bj.pos.y - bi.pos.y).normalize();

					float rij = bi.pos.distance(bj.pos);
					float fij = NBodyGame.gstrength > 50 ? 0f : (float) (Constants.G * bi.mass * bj.mass / Math.pow(
							Math.pow(rij, NBodyGame.gstrength) + Constants.eps2, 1.25));

					// Scale it by the force
					NBodyGame.fvec.scale(fij);

					// Check event repulsion and compute force
					if (NBodyGame.currentEvent == Event.REPEL) {
						if (bi.isMovable())
							bi.force.add(getRepulsionForce(bi));
						if (bj.isMovable())
							bj.force.add(getRepulsionForce(bj));
					}

					// Sum force of particle to partial force sum
					// Force direction depends on particle type

					if (isAttraction(bi, bj)) {
						if (bi.isMovable())
							bi.force.add(NBodyGame.fvec);
						if (bj.isMovable())
							bj.force.add(NBodyGame.fvec.scale(-1f));
					} else {
						if (bj.isMovable())
							bj.force.add(NBodyGame.fvec);
						if (bi.isMovable())
							bi.force.add(NBodyGame.fvec.scale(-1f));
					}

					if (NBodyGame.collisions != CollisionPolicy.NONE
							&& bi.distance(bj) <= bi.getEffectiveRadius() + bj.getEffectiveRadius()) {
						bi.addCollision(bj);
					}

					bj.treated = false;
				}
				bi.treated = false;
			}
		}

	}

	@Override
	public void initialize(float inix, float iniy, float width, float height, boolean displayGrid) {
		// Nothing to do here
	}

	/**
	 * Two bodies are attracted if they are the same type or if one of them is a black hole or a static particle. (i.e.
	 * BHs and Static particles
	 * attract both particles and antiparticles).
	 * 
	 * @param bi
	 * @param bj
	 * @return
	 */
	public boolean isAttraction(Body bi, Body bj) {
		return bi.starType == bj.starType || bi.starType == StarType.BLACKHOLE || bi.starType == StarType.STATIC
				|| bj.starType == StarType.BLACKHOLE || bj.starType == StarType.STATIC;
	}

}
