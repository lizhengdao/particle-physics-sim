package com.nbody.core.util;

/**
 * A general-purpose pool of objects. Objects in the pool are allocated up front and then
 * passed out to requesting objects until the pool is exhausted (at which point an error is thrown).
 * Code that requests objects from the pool should return them to the pool when they are finished.
 * This class is abstract; derivations need to implement the fill() function to fill the pool, and
 * may wish to override release() to clear state on objects as they are returned to the pool.
 * 
 * @author Toni Sagrista
 */
public abstract class ObjectPool {
	private FixedSizeArray<Object> available;
	protected int size;

	private static final int DEFAULT_SIZE = 32;

	public ObjectPool() {
		super();
		setSize(DEFAULT_SIZE);
	}

	public ObjectPool(int size) {
		super();
		setSize(size);
	}

	public void reset() {
		available.clear();
	}

	/** Allocates an object from the pool */
	protected Object allocate() {
		Object result = available.removeLast();
		assert result != null : "Object pool of type " + this.getClass().getSimpleName() + " exhausted!!";
		return result;
	}

	/** Returns an object to the pool. */
	public void release(Object entry) {
		available.add(entry);
	}

	/** Returns the number of pooled elements that have been allocated but not released. */
	public int getAllocatedCount() {
		return available.getCapacity() - available.getCount();
	}

	/**
	 * Returns the number of elements allocated but never used
	 * 
	 * @return
	 */
	public int getFreeCount() {
		return available.getCount();
	}

	private void setSize(int size) {
		this.size = size;
		available = new FixedSizeArray<Object>(this.size);

		fill();
	}

	protected abstract void fill();

	protected FixedSizeArray<Object> getAvailable() {
		return available;
	}

}
