package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.R;
import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.GLSprite;
import com.nbody.core.graph.TextureHandler;

/**
 * A cross in the screen
 * 
 * @author Toni Sagrista
 */
public class Cross extends GLSprite {

	public Cross(Vector2 pos) {
		super(pos, null);
		mTexture = TextureHandler.getTexture(R.drawable.cross);
	}

	public void setPosition(float x, float y) {
		pos.set(x, y);
	}

	public void setPosition(Vector2 newPos) {
		pos.set(newPos);
	}

	@Override
	public void draw(GL10 gl) {
		super.draw(gl, new float[] { 1f, 0f, 0f, 1f }, 1f);
	}

}
