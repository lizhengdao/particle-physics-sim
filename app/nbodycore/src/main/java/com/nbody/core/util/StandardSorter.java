package com.nbody.core.util;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Very basic implementation of sorter.
 * 
 * @author Toni Sagrista
 * 
 * @param <T>
 */
public class StandardSorter<T> extends Sorter {

	@Override
	public void sort(Object[] array, int count, Comparator comparator) {
		Arrays.sort(array, 0, count, comparator);
	}

}
