package com.nbody.core.game;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 0 - white
 * 1 - yellow
 * 2 - red
 * 3 - blue
 * 4 - mix
 * 
 * @author Toni Sagrista
 * 
 */
public class ColorHelper {
	private static final float opacity = .9f;
	private static final float dillution = .7f;
	private static Map<Integer, float[]> colors = new HashMap<Integer, float[]>();

	public static ColorHelper ch = null;

	public static void initialize(int mode) {
		ch = new ColorHelper(mode);
	}

	// Colors [red, green, blue, alpha]
	static {
		// white
		colors.put(0, new float[] { 1f, 1f, 1f, opacity });

		// yellow
		colors.put(1, new float[] { 1f, 1f, dillution, opacity });

		// red
		colors.put(2, new float[] { 1f, dillution, dillution, opacity });

		// blue
		colors.put(3, new float[] { dillution, dillution, 1f, opacity });

		// purple
		colors.put(4, new float[] { 204f / 255f, 168f / 255f, 217f / 255f, opacity });
	}

	int mode = 4;
	Random rng;

	public ColorHelper(int mode) {
		super();
		this.mode = mode;
		rng = new Random();
	}

	public int generateColor() {
		if (mode < 4)
			return mode;
		else
			return rng.nextInt(4);
	}

	public int getBHColor() {
		return 4;
	}

	public float[] getColor(int col) {
		return colors.get(col);
	}

	public float[] getColor(int col, int alpha) {
		float[] aux = colors.get(col).clone();
		aux[3] = alpha;
		return aux;
	}

}
