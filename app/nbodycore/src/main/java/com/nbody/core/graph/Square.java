package com.nbody.core.graph;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

/**
 * A high-level square that can be rendered in OpenGL.
 * 
 * @author Toni Sagrista
 * 
 */
public class Square {
	float side = 1f;
	float offset = .001f;
	// Our vertices.
	private float[] vertices;

	// The order we like to connect them.
	private short[] indices = { 0, 1, 2, 3, 0 };

	// Our vertex buffer.
	private FloatBuffer vertexBuffer;

	// Our index buffer.
	private ShortBuffer indexBuffer;

	public Square() {
		this(1f);
	}

	public Square(float wToHRatio) {
		this(wToHRatio, 1);
	}

	public Square(float wToHRatio, float side) {
		this.side = side;
		wToHRatio *= side;
		initVertices(wToHRatio);

		// a float is 4 bytes, therefore we multiply the number if
		// vertices with 4.
		ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length * 4);
		vbb.order(ByteOrder.nativeOrder());
		vertexBuffer = vbb.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);

		// short is 2 bytes, therefore we multiply the number if
		// vertices with 2.
		ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length * 2);
		ibb.order(ByteOrder.nativeOrder());
		indexBuffer = ibb.asShortBuffer();
		indexBuffer.put(indices);
		indexBuffer.position(0);
	}

	private void initVertices(float ratio) {
		vertices = new float[] { -side / 2f - offset, -ratio / 2f - offset, 0.0f, // 0, Top Left
				-side / 2f - offset, ratio / 2f + offset, 0.0f, // 1, Bottom Left
				side / 2f + offset, ratio / 2f + offset, 0.0f, // 2, Bottom Right
				side / 2f + offset, -ratio / 2f - offset, 0.0f, // 3, Top Right
		};
	}

	/**
	 * This function draws our square on screen.
	 * 
	 * @param gl
	 */
	public void draw(GL10 gl) {
		draw(gl, GL10.GL_LINE_STRIP);
	}

	/**
	 * This function draws our square on screen.
	 * 
	 * @param gl
	 */
	public void draw(GL10 gl, int drawType) {
		// Counter-clockwise winding.
		// gl.glFrontFace(GL10.GL_CCW); // OpenGL docs
		// Enable face culling.
		// gl.glEnable(GL10.GL_CULL_FACE); // OpenGL docs
		// What faces to remove with the face culling.
		// gl.glCullFace(GL10.GL_BACK); // OpenGL docs

		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);// OpenGL docs.
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, // OpenGL docs
				vertexBuffer);

		gl.glDrawElements(drawType, indices.length,// OpenGL docs
				GL10.GL_UNSIGNED_SHORT, indexBuffer);

		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY); // OpenGL docs
		// Disable face culling.
		// gl.glDisable(GL10.GL_CULL_FACE); // OpenGL docs
	}

}
