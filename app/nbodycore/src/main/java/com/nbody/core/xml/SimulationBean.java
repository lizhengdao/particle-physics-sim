package com.nbody.core.xml;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean carrying information of a simulation.
 * 
 * @author Toni Sagrista
 * 
 */
public class SimulationBean {

	// Particles
	public List<ParticleBean> particles;

	// Walls
	public List<WallBean> walls;

	// Number of particles
	public int N;

	// Velcity
	public float dtscale;

	// Boundary conditions
	public boolean bc;

	// Collision policy
	public String collisions;

	// Particle color policy
	public int colorPolicy;

	// Show sparks
	public boolean showSparks;

	// Gravity strength
	public float gstrength;

	// Accelerometer enabled
	public boolean accelerometer;

	// Restore particles
	public boolean restore;

	// BG color
	public int bgColor;

	// BG image
	public String bgImage;

	// Simulation area screen
	public boolean simAreaScreen;

	// BH sprite
	public int bhSprite;

	// Star sprtie
	public int starSprite;

	// BH
	public boolean bh;

	// Force calculator method
	public String forceMethod;

	// Display grid
	public boolean displayGrid;

	// Display mesh points
	public boolean displayMeshPoints;

	// Mesh density
	public int meshDensity;

	// Show tail
	public boolean showTail;

	// Tail length
	public int ntail;

	// Particle size
	public float particleMass;

	// Friction
	public String friction;

	/**
	 * Default creator
	 */
	public SimulationBean() {
		particles = new ArrayList<ParticleBean>();
		walls = new ArrayList<WallBean>();
	}

}
