package com.nbody.core.game;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.RegularPolygon;
import com.nbody.core.graph.Renderable;
import com.nbody.core.util.PolygonPool;
import com.nbody.core.util.VectorPool;

/**
 * The sparks effect when particles collide or merge.
 * 
 * @author Toni Sagrista
 * 
 */
public class Sparks extends Renderable {
	float scale, velocity = .6f, scaleAccel;
	RegularPolygon polygon;

	FloatBuffer mVertexBuffer;
	ShortBuffer mIndexBuffer;
	int mIndexNumber;

	public Sparks(Vector2 pos, float scale, float scaleAccel) {
		this(pos, scale, scaleAccel, 25);
	}

	public Sparks(Vector2 pos, float scale, float scaleAccel, int sides) {
		this.pos = pos;
		this.scaleAccel = scaleAccel;
		this.scale = scale;

		polygon = PolygonPool.pool.allocate(0f, 0f, 0f, 5f, sides);
		mVertexBuffer = polygon.getVertexBuffer();
		mIndexBuffer = polygon.getIndexBuffer();
		mIndexNumber = polygon.getNumberOfIndecies();
	}

	public void update(float dt) {
		velocity += scaleAccel * dt;
		if (velocity < 0)
			velocity = 0;
		scale += velocity * dt;
	}

	public boolean finished() {
		return velocity == 0;
	}

	public void release() {
		VectorPool.pool.release(this.pos);
		PolygonPool.pool.release(this.polygon);
	}

	@Override
	public void draw(GL10 gl) {
		gl.glPushMatrix();
		gl.glColor4f(1f, 1f, 1f, .05f);
		gl.glTranslatef(pos.x, pos.y, 0);
		gl.glScalef(scale, scale, 1f);

		// Enabled the vertices buffer for writing and to be used during
		// rendering.
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);// OpenGL docs.
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.

		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, // OpenGL docs
				mVertexBuffer.position(0));
		gl.glDrawElements(GL10.GL_TRIANGLE_FAN, mIndexNumber, GL10.GL_UNSIGNED_SHORT, mIndexBuffer.position(0));
		// Disable the vertices buffer.
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY); // OpenGL docs

		gl.glPopMatrix();

	}
}
