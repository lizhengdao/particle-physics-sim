package com.nbody.core.geom;

/**
 * A rectangle used to determine bounds
 * 
 * @author Toni Sagrista
 * 
 */
public class Rectangle {
	private float x;
	private float y;
	/** The width of the box */
	protected float width;
	/** The height of the box */
	protected float height;

	protected float maxX;
	protected float maxY;

	/**
	 * Create a new bounding box
	 * 
	 * @param x
	 *            The x position of the box
	 * @param y
	 *            The y position of the box
	 * @param width
	 *            The width of the box
	 * @param height
	 *            The hieght of the box
	 */
	public Rectangle(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		maxX = x + width;
		maxY = y + height;
	}

	/**
	 * Check if this rectangle contains a point
	 * 
	 * @param xp
	 *            The x coordinate of the point to check
	 * @param yp
	 *            The y coordinate of the point to check
	 * @return True if the point is within the rectangle
	 */
	public boolean contains(float xp, float yp) {
		if (xp <= x) {
			return false;
		}
		if (yp <= y) {
			return false;
		}
		if (xp >= maxX) {
			return false;
		}
        return !(yp >= maxY);
    }

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

}
