package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;

import com.nbody.core.game.NBodyGame.Event;
import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.Square;
import com.nbody.core.util.FixedSizeArray;
import com.nbody.core.util.VectorPool;
import com.nbody.core.util.math.BiCubicSplineFirstDerivative;
import com.nbody.core.util.math.LinearInterpolator;

/**
 * Force calculator for the particle-mesho method.
 * 
 * @author Toni Sagrista
 * 
 */
public class ForcePM extends IForceCalculator {

	private static int mResolution = 6; // The number of points per side. Final efficiency will be O(n*resolution^2)
										// where n is number of particles
	private static int mDrawResolution = 2; // Squares per cell in drawing (side)

	/**
	 * fieldLength - Length of the mass field that a particle exerts on mesh vertices
	 * cellArea - Area of a cell in the mesh
	 * fieldArea - Area of the mass field of a particle
	 */
	private static float fieldLength, cellArea, fieldArea, squareLength;
	private static int cellRes, sideSquares;
	private static Cell[][] cells;
	private static MeshPoint[][] meshPoints;
	private static float[][] xy;
	private static float wstep, hstep;
	private static BiCubicSplineFirstDerivative spline;
	private static Cross cross;

	private static final float hmin = -2000000f, hmax = 9E7f;

	private static ScreenSquare[][] squares;

	public ForcePM() {
		super();
	}

	public ForcePM(int resolution) {
		super();
		mResolution = resolution;
	}

	@Override
	public void initialize(float inix, float iniy, float width, float height, boolean displayGrid) {
		/**
		 * Remember that we add 2 to cellRes when we calculate the fieldLength, the wstep and the hstep
		 * because we want the mesh points to be inside the simulation area and not on its boundary.
		 */

		cellRes = mResolution - 1;
		fieldLength = width / ((float) cellRes * 2f); // Half the size of the width of the cell
		// Calculate cell width and height
		wstep = width / (float) cellRes;
		hstep = height / (float) cellRes;
		// Calculate cell area
		cellArea = wstep * hstep;
		fieldArea = fieldLength * fieldLength;

		// Initialize cells
		float x = inix, y = iniy;
		cells = new Cell[cellRes][cellRes];
		for (int i = 0; i < cellRes; i++) {
			for (int j = 0; j < cellRes; j++) {
				// Initialize

				// Remember cells share vertices!
				Cell up = (j > 0) ? cells[i][j - 1] : null;
				Cell left = (i > 0) ? cells[i - 1][j] : null;

				cells[i][j] = new Cell(x, y, wstep, hstep, up, left);

				// Advance y
				y += hstep;
			}
			// Advance x, reset y
			x += wstep;
			y = iniy;
		}

		// Initialize meshPoints references matrix
		meshPoints = getMeshPointMap();

		// Init points
		xy = getXYs();

		// Init spline
		spline = new BiCubicSplineFirstDerivative();

		if (displayGrid) {
			initDraw(inix, iniy, width, height);
		}
		if (NBodyGame.displayMeshPoints) {
			cross = new Cross(VectorPool.pool.allocate(0f, 0f));
		}
	}

	private void initDraw(float inix, float iniy, float width, float height) {
		sideSquares = cellRes * mDrawResolution;
		squareLength = width / sideSquares;
		squares = new ScreenSquare[sideSquares][sideSquares];
		float squareW = width / (float) sideSquares;
		float squareH = height / (float) sideSquares;
		float WtoHratio = squareH / squareW;

		float x = inix + squareW / 2f, y = iniy + squareH / 2f;
		for (int i = 0; i < sideSquares; i++) {
			for (int j = 0; j < sideSquares; j++) {
				squares[i][j] = new ScreenSquare(x, y, new Square(WtoHratio));
				y += squareH;
			}
			y = iniy + squareH / 2f;
			x += squareW;
		}
	}

	@Override
	/**
	 * Draw mesh densities
	 */
	public void draw(GL10 gl) {
		for (int i = 0; i < sideSquares; i++) {
			for (int j = 0; j < sideSquares; j++) {
				ScreenSquare ss = squares[i][j];
				try {
					float height = spline.interpolate(ss.x, ss.y)[0];
					// Convert height to color!

					float color = 0f;
					if (height > hmax)
						color = .9f;
					else {
						LinearInterpolator li = new LinearInterpolator(new float[] { hmin, hmax }, new float[] { .05f,
								.9f });
						color = li.value(height);
					}

					gl.glColor4f(0f, color, 0f, 1f);
					gl.glTranslatef(ss.x, ss.y, 0f);
					gl.glScalef(squareLength, squareLength, 1f);

					ss.square.draw(gl, GL10.GL_TRIANGLE_FAN);

					gl.glScalef(1f / squareLength, 1f / squareLength, 1f);
					gl.glTranslatef(-ss.x, -ss.y, 0f);
				} catch (Exception e) {
				}
			}
		}
	}

	public void drawMeshPoints(GL10 gl) {
		for (int i = 0; i < mResolution; i++) {
			for (int j = 0; j < mResolution; j++) {
				MeshPoint mp = meshPoints[i][j];
				cross.setPosition(mp.pos);
				cross.draw(gl);
			}
		}
	}

	@Override
	public int getVectorsUsed() {
		return mResolution * mResolution;
	}

	@Override
	public void calculateForces(FixedSizeArray<Body> bodies) {
		// Update densities in the mesh
		updateDensities(bodies);

		// Update bicubic spline map

		spline.reset(xy[0], xy[1], getDensities());

		// Now, for each body, calculate height in mesh and derivatives to know where trajectory is going
		for (int k = 0; k < bodies.mCount; k++) {
			Body bi = bodies.get(k);
			try {
				float[] interp = spline.interpolate(bi.pos.x, bi.pos.y);
				float height = interp[0];
				float dx = interp[1];
				float dy = interp[2];

				// We're all set
				NBodyGame.fvec.set(dx, dy).normalize().scale(Math.abs(height) * 2E1f);

				// Check event repulsion and compute force
				if (NBodyGame.currentEvent == Event.REPEL) {
					if (bi.isMovable())
						bi.force.add(getRepulsionForce(bi));
				}
				// Sum force of particle to partial force sum
				if (bi.isMovable())
					bi.force.add(NBodyGame.fvec);
				bi.treated = false;
			} catch (Exception e) {

			}
		}

	}

	/**
	 * Returns a list of Xs and a list of Ys in the mesh
	 * 
	 * @return
	 *         res[0] - List of Xs
	 *         res[1] - List of Ys
	 */
	private float[][] getXYs() {
		float[][] res = new float[2][mResolution];

		// Set Xs
		for (int i = 0; i < mResolution; i++) {
			MeshPoint mp = meshPoints[i][i];
			res[0][i] = mp.pos.x;
			res[1][i] = mp.pos.y;
		}

		return res;
	}

	/**
	 * Returns a list with the densities of the meshPoints
	 * 
	 * @return
	 */
	private float[][] getDensities() {
		float[][] res = new float[mResolution][mResolution];
		for (int i = 0; i < mResolution; i++) {
			for (int j = 0; j < mResolution; j++) {
				res[i][j] = meshPoints[i][j].density;
			}
		}
		return res;
	}

	/**
	 * Returns the mesh points in a map
	 * 
	 * @return
	 */
	private MeshPoint[][] getMeshPointMap() {
		MeshPoint[][] res = new MeshPoint[mResolution][mResolution];

		// Iterate over cells and populate res
		for (int i = 0; i < cellRes; i++) {
			for (int j = 0; j < cellRes; j++) {
				res[i][j] = (res[i][j] == null) ? cells[i][j].points[0][0] : res[i][j];
				res[i + 1][j] = (res[i + 1][j] == null) ? cells[i][j].points[1][0] : res[i + 1][j];
				res[i][j + 1] = (res[i][j + 1] == null) ? cells[i][j].points[0][1] : res[i][j + 1];
				res[i + 1][j + 1] = (res[i + 1][j + 1] == null) ? cells[i][j].points[1][1] : res[i + 1][j + 1];
			}
		}

		return res;
	}

	/**
	 * Gets the cell this body is currently in
	 * 
	 * @param body
	 * @return
	 */
	private Cell getCell(Body body) {
		int i = (int) ((body.pos.x + NBodyGame.boundary.getWidth() / 2) / wstep);
		int j = (int) ((body.pos.y + NBodyGame.boundary.getHeight() / 2) / hstep);
		if (i < 0)
			i = 0;
		if (i > cellRes - 1)
			i = cellRes - 1;
		if (j < 0)
			j = 0;
		if (j > cellRes - 1)
			j = cellRes - 1;
		return cells[i][j];
	}

	private void updateDensities(FixedSizeArray<Body> bodies) {
		// Reset
		for (int i = 0; i < cellRes; i++) {
			for (int j = 0; j < cellRes; j++) {
				cells[i][j].resetDensities();
			}
		}
		// Update
		for (int i = 0; i < bodies.mCount; i++) {
			Body body = bodies.get(i);
			getCell(body).updateDensities(body);
		}
	}

	/**
	 * Represents a cell in the mesh in order to implement the particle-mesh (PM) code
	 * 
	 * @author toninoni
	 * 
	 */
	private class Cell {

		MeshPoint[][] points;

		/**
		 * Creates a cell given the position of the upper-left vertex and the
		 * width and height of the cell
		 * 
		 * i, j ________________ i+1, j
		 * [0,0] | | [1,0]
		 * | .[x,y] |
		 * | |
		 * | |
		 * | |
		 * | |
		 * | |
		 * ________________
		 * i, j+1 i+1, j+1
		 * [0,1] [1,1]
		 * 
		 * @param x
		 * @param y
		 */
		public Cell(float x, float y, float cellw, float cellh, Cell up, Cell left) {
			super();
			points = new MeshPoint[2][2];
			// UL - the only possible point shared by cells up and left
			points[0][0] = (up == null) ? ((left == null) ? new MeshPoint(x, y) : left.points[1][0])
					: (up.points[0][1]);
			// BL - this point is already created if left is not null
			points[0][1] = left == null ? new MeshPoint(x, y + cellh) : left.points[1][1];
			// UR - this point is already created if up is not null
			points[1][0] = up == null ? new MeshPoint(x + cellw, y) : up.points[1][1];
			// BR
			points[1][1] = new MeshPoint(x + cellw, y + cellh);
		}

		public void updateDensities(Body body) {
			// Calculate coordinates of particle relative to cell UL corner
			float relx = body.pos.x - points[0][0].pos.x;
			float rely = body.pos.y - points[0][0].pos.y;

			float incxi = wstep / 2 - (relx - fieldLength / 2);
			float incxi1 = fieldLength - incxi;

			float incyj = hstep / 2 - (rely - fieldLength / 2);
			float incyj1 = fieldLength - incyj;

			float rho_c = body.mass / fieldArea;

			/** Calculate contribution to mesh point i,j -> [0,0] **/
			points[0][0].density += (getIntersectionArea(incxi, incyj) / cellArea) * rho_c
					* (((wstep - relx) * (hstep - rely)) / (wstep * hstep));

			/** Calculate contribution to mesh point i+1,j -> [1,0] **/
			points[1][0].density += (getIntersectionArea(incxi1, incyj) / cellArea) * rho_c
					* ((relx * (hstep - rely)) / (wstep * hstep));

			/** Calculate contribution to mesh point i,j+1 -> [0,1] **/
			points[0][1].density += (getIntersectionArea(incxi, incyj1) / cellArea) * rho_c
					* (((wstep - relx) * rely) / (wstep * hstep));

			/** Calculate contribution to mesh point i+1,j+1 -> [1,1] **/
			points[1][1].density += (getIntersectionArea(incxi1, incyj1) / cellArea) * rho_c
					* ((relx * rely) / (wstep * hstep));

		}

		private float getIntersectionArea(float incx, float incy) {
			return (incx <= 0 || incy <= 0) ? 0 : incx * incy;
		}

		public void resetDensities() {
			points[0][0].density = 0;
			points[0][1].density = 0;
			points[1][0].density = 0;
			points[1][1].density = 0;
		}

		@Override
		public String toString() {
			String result = "";
			for (MeshPoint[] lvl1 : points)
				for (MeshPoint point : lvl1)
					result += point.toString();

			return result;
		}

	}

	/**
	 * Represents a mesh point, a mere information container. It just holds the exact position of the mesh point
	 * and the density, which is updated at every time step
	 * 
	 * @author toninoni
	 * 
	 */
	private class MeshPoint {
		Vector2 pos;
		float density;

		public MeshPoint(float x, float y) {
			pos = VectorPool.pool.allocate(x, y);
			density = 0;
		}

		@Override
		public String toString() {
			return pos != null ? pos.toString() : "[]";
		}

	}

	private class ScreenSquare {
		Square square;
		float x, y;

		public ScreenSquare(float x, float y, Square square) {
			this.x = x;
			this.y = y;
			this.square = square;
		}

		public void draw(GL10 gl) {
			square.draw(gl);
		}
	}
}
