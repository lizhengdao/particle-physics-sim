package com.nbody.core.xml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

/**
 * BaseParser implementation.
 * 
 * @author Toni Sagrista
 * 
 */
public class DomParser extends BaseParser {

	public DomParser() {
		super();
	}

	public SimulationBean parseXml(File file) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		SimulationBean sb = new SimulationBean();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document dom = builder.parse(new FileInputStream(file));
			Element root = dom.getDocumentElement();
			Node simulation = root.getElementsByTagName(TAG_SIMULATION).item(0);
			NamedNodeMap attributes = simulation.getAttributes();
			sb.N = toInteger(attributes.getNamedItem("N").getNodeValue());
			sb.dtscale = toFloat(attributes.getNamedItem("dtscale").getNodeValue());
			sb.bc = toBoolean(attributes.getNamedItem("bc").getNodeValue());
			sb.collisions = attributes.getNamedItem("collisions").getNodeValue();
			sb.colorPolicy = toInteger(attributes.getNamedItem("colorPolicy").getNodeValue());
			sb.showSparks = toBoolean(attributes.getNamedItem("showSparks").getNodeValue());
			sb.gstrength = toFloat(attributes.getNamedItem("gstrength").getNodeValue());
			sb.accelerometer = toBoolean(attributes.getNamedItem("accelerometer").getNodeValue());
			sb.restore = toBoolean(attributes.getNamedItem("restore").getNodeValue());
			sb.bgColor = toInteger(attributes.getNamedItem("bgColor").getNodeValue());
			if (attributes.getNamedItem("bgImage") != null)
				sb.bgImage = attributes.getNamedItem("bgImage").getNodeValue();
			sb.simAreaScreen = toBoolean(attributes.getNamedItem("simAreaScreen").getNodeValue());
			sb.bhSprite = toInteger(attributes.getNamedItem("bhSprite").getNodeValue());
			sb.bh = toBoolean(attributes.getNamedItem("bh").getNodeValue());
			sb.forceMethod = attributes.getNamedItem("forceMethod").getNodeValue();
			sb.displayGrid = toBoolean(attributes.getNamedItem("displayGrid").getNodeValue());
			sb.displayMeshPoints = toBoolean(attributes.getNamedItem("displayMeshPoints").getNodeValue());
			sb.meshDensity = toInteger(attributes.getNamedItem("meshDensity").getNodeValue());
			sb.showTail = toBoolean(attributes.getNamedItem("showTail").getNodeValue());
			sb.ntail = toInteger(attributes.getNamedItem("ntail").getNodeValue());
			sb.particleMass = toFloat(attributes.getNamedItem("particleMass").getNodeValue());
			sb.friction = attributes.getNamedItem("friction").getNodeValue();
			// Ensure backwards compat
			if (attributes.getNamedItem("starSprite") != null)
				sb.starSprite = toInteger(attributes.getNamedItem("starSprite").getNodeValue());
			else
				sb.starSprite = 0;

			Node particles = simulation.getChildNodes().item(0);
			Node walls = simulation.getChildNodes().item(1);

			NodeList nl = particles.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				ParticleBean pb = new ParticleBean();
				pb.bh = toBoolean(n.getAttributes().getNamedItem("bh").getNodeValue());
				pb.starType = n.getAttributes().getNamedItem("starType").getNodeValue();

				pb.radius = toFloat(n.getAttributes().getNamedItem("radius").getNodeValue());
				pb.mass = toFloat(n.getAttributes().getNamedItem("mass").getNodeValue());
				pb.color = toInteger(n.getAttributes().getNamedItem("color").getNodeValue());

				pb.x = toFloat(n.getAttributes().getNamedItem("x").getNodeValue());
				pb.y = toFloat(n.getAttributes().getNamedItem("y").getNodeValue());
				pb.vx = toFloat(n.getAttributes().getNamedItem("vx").getNodeValue());
				pb.vy = toFloat(n.getAttributes().getNamedItem("vy").getNodeValue());

				sb.particles.add(pb);
			}

			nl = walls.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				WallBean wb = new WallBean();

				wb.color = toInteger(n.getAttributes().getNamedItem("color").getNodeValue());

				wb.x1 = toFloat(n.getAttributes().getNamedItem("x1").getNodeValue());
				wb.y1 = toFloat(n.getAttributes().getNamedItem("y1").getNodeValue());
				wb.x2 = toFloat(n.getAttributes().getNamedItem("x2").getNodeValue());
				wb.y2 = toFloat(n.getAttributes().getNamedItem("y2").getNodeValue());

				sb.walls.add(wb);
			}
		} catch (Exception e) {
			Log.e(this.getClass().getName(), "Exception parsing file", e);
			return null;
		}
		return sb;
	}

	@Override
	public boolean writeXml(SimulationBean bean, File file) {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try {
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			serializer.startTag("", TAG_SIMULATION);
			add(serializer, "N", bean.N);
			add(serializer, "dtscale", bean.dtscale);
			add(serializer, "bc", bean.bc);
			add(serializer, "collisions", bean.collisions);
			add(serializer, "colorPolicy", bean.colorPolicy);
			add(serializer, "showSparks", bean.showSparks);
			add(serializer, "gstrength", bean.gstrength);
			add(serializer, "accelerometer", bean.accelerometer);
			add(serializer, "restore", bean.restore);
			add(serializer, "bgColor", bean.bgColor);
			add(serializer, "bgImage", bean.bgImage);
			add(serializer, "simAreaScreen", bean.simAreaScreen);
			add(serializer, "bhSprite", bean.bhSprite);
			add(serializer, "bh", bean.bh);
			add(serializer, "starSprite", bean.starSprite);
			add(serializer, "forceMethod", bean.forceMethod);
			add(serializer, "displayGrid", bean.displayGrid);
			add(serializer, "displayMeshPoints", bean.displayMeshPoints);
			add(serializer, "meshDensity", bean.meshDensity);
			add(serializer, "showTail", bean.showTail);
			add(serializer, "ntail", bean.ntail);
			add(serializer, "particleMass", bean.particleMass);
			add(serializer, "friction", bean.friction);

			serializer.startTag("", TAG_PARTICLES);
			for (ParticleBean p : bean.particles) {
				serializer.startTag("", TAG_PARTICLE);
				add(serializer, "bh", p.bh);
				add(serializer, "starType", p.starType);
				add(serializer, "radius", p.radius);
				add(serializer, "mass", p.mass);
				add(serializer, "color", p.color);
				add(serializer, "x", p.x);
				add(serializer, "y", p.y);
				add(serializer, "vx", p.vx);
				add(serializer, "vy", p.vy);
				serializer.endTag("", TAG_PARTICLE);
			}
			serializer.endTag("", TAG_PARTICLES);

			serializer.startTag("", TAG_WALLS);
			for (WallBean p : bean.walls) {
				serializer.startTag("", TAG_WALL);
				add(serializer, "color", p.color);
				add(serializer, "x1", p.x1);
				add(serializer, "y1", p.y1);
				add(serializer, "x2", p.x2);
				add(serializer, "y2", p.y2);
				serializer.endTag("", TAG_WALL);
			}
			serializer.endTag("", TAG_WALLS);

			serializer.endTag("", TAG_SIMULATION);
			serializer.endDocument();

			// Now, write to file
			try {
				if (file.exists()) {
					Log.w(this.getClass().getName(), "File " + file.getAbsolutePath() + " already exists, overwritting");
					file.delete();
				}
				file.createNewFile();
				if (file.canWrite() && file.isFile()) {
					FileWriter filewriter = new FileWriter(file);
					BufferedWriter out = new BufferedWriter(filewriter);
					out.append(writer.toString());
					out.close();
				}
			} catch (IOException e) {
				Log.e(this.getClass().getName(), "Could not write file " + e.getMessage(), e);
				return false;
			}

		} catch (Exception e) {
			Log.e(this.getClass().getName(), "Error creating xml", e);
			return false;
		}
		return true;
	}

	private void add(XmlSerializer serializer, String tag, String value) throws IllegalArgumentException,
			IllegalStateException, IOException {
		if (value != null)
			serializer.attribute("", tag, value);
	}

	private void add(XmlSerializer serializer, String tag, Float value) throws IllegalArgumentException,
			IllegalStateException, IOException {
		if (value != null)
			serializer.attribute("", tag, toString(value));
	}

	private void add(XmlSerializer serializer, String tag, Integer value) throws IllegalArgumentException,
			IllegalStateException, IOException {
		if (value != null)
			serializer.attribute("", tag, toString(value));
	}

	private void add(XmlSerializer serializer, String tag, Boolean value) throws IllegalArgumentException,
			IllegalStateException, IOException {
		if (value != null)
			serializer.attribute("", tag, toString(value));
	}

	private String toString(Float f) {
		return f.toString();
	}

	private String toString(Integer i) {
		return i.toString();
	}

	private String toString(Boolean b) {
		return b.toString();
	}

	private Float toFloat(String s) {
		return Float.parseFloat(s);
	}

	private Integer toInteger(String s) {
		return Integer.parseInt(s);
	}

	private Boolean toBoolean(String s) {
		return Boolean.parseBoolean(s);
	}
}
