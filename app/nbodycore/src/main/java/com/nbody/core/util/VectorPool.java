package com.nbody.core.util;

import android.util.Log;

import com.nbody.core.geom.Vector2;

/**
 * A pool of 2D vectors.
 * 
 * @author Toni Sagrista
 */
public class VectorPool extends TObjectPool<Vector2> {

	public static VectorPool pool;

	public static void initVectorPool(int size) {
		if (pool != null) {
			// release previously loaded pool
			pool.reset();
			System.gc();
		}
		pool = new VectorPool(size);
		// pool.logUsed();
	}

	private VectorPool(int size) {
		super(size);
		fill();
	}

	public VectorPool() {
		super();
	}

	@Override
	protected void fill() {
		for (int x = 0; x < size; x++) {
			getAvailable().add(new Vector2());
		}
	}

	@Override
	public void release(Object entry) {
		((Vector2) entry).zero();
		super.release(entry);
		// logUsed();
	}

	/** Allocates a vector and assigns the value of the passed source vector to it. */
	public Vector2 allocate(Vector2 source) {
		Vector2 entry = super.allocate();
		entry.set(source);
		// logUsed();
		return entry;
	}

	public Vector2 allocate(float x, float y) {
		Vector2 entry = super.allocate();
		if (entry == null) {
			Log.e("VectorPool", "VectorPool full!");
			throw new RuntimeException("Pool is full, can't accomodate more Vectors");
		} else {
			entry.set(x, y);
			// logUsed();
			return entry;
		}
	}

	private void logUsed() {
		Log.d("VectorPool", "Used: " + this.getAllocatedCount() + "\t Remaining: " + this.getFreeCount());
	}

}
