package com.nbody.core.util;

/**
 * Locks for concurrent access to different kinds of resources.
 * 
 * @author Toni Sagrista
 * 
 */
public class Locks {
	public Object renderLockBody;
	public Object renderLockGeom;
	public Object initLock;

	public static Locks l = new Locks();

	public Locks() {
		renderLockBody = new Object();
		renderLockGeom = new Object();
		initLock = new Object();
	}
}
