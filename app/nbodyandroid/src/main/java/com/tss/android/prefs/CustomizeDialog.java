package com.tss.android.prefs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.tss.android.GameThread;
import com.tss.android.NBodyActivity;
import com.tss.android.R;

/** Class Must extend Dialog */
/** Implement onClickListener to dismiss dialog when OK Button is pressed */
public class CustomizeDialog extends Dialog implements OnDismissListener, OnClickListener {
	GLSurfaceView view;
	GameThread thread;

	public CustomizeDialog(Context context, GLSurfaceView view,
			GameThread thread) {
		super(context);
		setTitle(String.format(
				context.getResources().getString(R.string.abouttitle),
				NBodyActivity.versionName));
		/** Design the dialog in about.xml file */
		setContentView(R.layout.about);

		this.setOnDismissListener(this);

		this.view = view;
		this.thread = thread;
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public void onDismiss(DialogInterface arg0) {
		dismiss();

		thread.unpause();
		view.onResume();

	}

}
