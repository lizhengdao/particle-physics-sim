package com.tss.android;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;

import com.nbody.core.game.NBodyGame.CollisionPolicy;
import com.nbody.core.game.NBodyGame.ForceMethod;
import com.nbody.core.util.Constants;
import com.tss.android.prefs.ColorPickerDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class SetPrefs extends PreferenceActivity implements OnPreferenceClickListener, OnPreferenceChangeListener,
        ColorPickerDialog.OnColorChangedListener {

    private static final String KEY_BG_IMG_PATH = "lastBgImgPath";
    private static final String NBODY_DIR = "com.tss.android/";

    private Uri mImageCaptureUri;
    private String mLastBgImgPath = null;
    //This must be a power of two
    private static int bgSize = 128;

    //YOU CAN EDIT THIS TO WHATEVER YOU WANT
    private static final int PICK_FROM_FILE = 1;
    private static final int CROP_FROM_CAMERA = 2;

    public static int changes = 0;
    SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            changes++;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Reset changes
        changes = 0;
        //This to count the number of changes made
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        prefs.registerOnSharedPreferenceChangeListener(listener);

        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        this.findPreference(Constants.PREF_BGCOLOR).setOnPreferenceClickListener(this);
        this.findPreference(Constants.PREF_BGIMAGE).setOnPreferenceClickListener(this);

        this.findPreference(Constants.PREF_COLLISIONS).setOnPreferenceChangeListener(this);
        this.findPreference(Constants.PREF_FORCEMETHOD).setOnPreferenceChangeListener(this);

        this.findPreference(Constants.PREF_BOUNDARY).setOnPreferenceClickListener(this);

        updateForceMethodState(null);
        updateRestoreParticlesState();
        updateBackgroundPreferencesState();
    }

    /**
     * OnClick handler
     */
    @Override
    public boolean onPreferenceClick(Preference pref) {
        if (pref.getKey().equals(Constants.PREF_BGCOLOR)) {
            SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            new ColorPickerDialog(this, this, Constants.PREF_BGCOLOR, mPrefs.getInt(Constants.PREF_BGCOLOR, Color.BLACK), Color.BLACK).show();
        } else if (pref.getKey().equals(Constants.PREF_BOUNDARY)) {
            updateRestoreParticlesState();
        } else if (pref.getKey().equals(Constants.PREF_BGIMAGE)) {
            launchBackgroundImageActivity();
        }
        return true;
    }

    /**
     * OnChange handler
     */
    @Override
    public boolean onPreferenceChange(Preference pref, Object newValue) {
        if (pref.getKey().equals(Constants.PREF_COLLISIONS))
            updateCollisionsState((String) newValue);
        else if (pref.getKey().equals(Constants.PREF_FORCEMETHOD))
            updateForceMethodState((String) newValue);
        return true;
    }

    @Override
    public void colorChanged(String key, int color) {
        this.getPreferenceScreen().getEditor().putInt(key, color).commit();
        //Reset background!
        this.getPreferenceScreen().getEditor().putString(Constants.PREF_BGIMAGE, "").commit();

        updateBackgroundPreferencesState();
    }

    private void updateForceMethodState(String newValue) {
        ListPreference p = (ListPreference) getPreferenceScreen().findPreference(Constants.PREF_FORCEMETHOD);
        if (newValue == null)
            newValue = p.getValue();
        getPreferenceScreen().findPreference(Constants.PREF_COLLISIONS).setEnabled(newValue.equalsIgnoreCase(ForceMethod.DIRECT.toString()));
        getPreferenceScreen().findPreference(Constants.PREF_DISPLAYGRID).setEnabled(newValue.equalsIgnoreCase(ForceMethod.PM.toString()));
        getPreferenceScreen().findPreference(Constants.PREF_DISPLAYMESHPOINTS).setEnabled(newValue.equalsIgnoreCase(ForceMethod.PM.toString()));
        getPreferenceScreen().findPreference(Constants.PREF_MESHDENSITY).setEnabled(newValue.equalsIgnoreCase(ForceMethod.PM.toString()));

        if (!newValue.equalsIgnoreCase(ForceMethod.DIRECT.toString())) {
            getPreferenceScreen().findPreference(Constants.PREF_SHOWSPARKS).setEnabled(false);
        } else {

            updateCollisionsState(null);
        }
    }

    private void updateCollisionsState(String newValue) {
        ListPreference p = (ListPreference) getPreferenceScreen().findPreference(Constants.PREF_COLLISIONS);
        if (newValue == null)
            newValue = p.getValue();
        getPreferenceScreen().findPreference(Constants.PREF_SHOWSPARKS).setEnabled(
                newValue.equalsIgnoreCase(CollisionPolicy.ELASTIC.toString()) || newValue.equalsIgnoreCase(CollisionPolicy.MERGERS.toString()));
    }

    private void updateRestoreParticlesState() {
        CheckBoxPreference p = (CheckBoxPreference) getPreferenceScreen().findPreference(Constants.PREF_BOUNDARY);
        getPreferenceScreen().findPreference(Constants.PREF_RESTOREPARTICLES).setEnabled(!p.isChecked());
    }

    private void launchBackgroundImageActivity() {
        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
    }

    //UPDATED
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_FROM_FILE) {

                mImageCaptureUri = data.getData();
                mLastBgImgPath = getPath(mImageCaptureUri);

                //doCrop();
                Bitmap source = BitmapFactory.decodeFile(mLastBgImgPath);
                int shortestSize = source.getHeight();
                if (source.getWidth() < shortestSize)
                    shortestSize = source.getWidth();
                //OpenGL ES can only load images whose size is a power of two
                bgSize = getMinimumPowerOfTwo(shortestSize);
                if (bgSize > 1024)
                    bgSize = 1024;

                //Work out offset in x and y
                float offsetX = ((float) (source.getWidth() - bgSize)) / 2;
                float offsetY = ((float) (source.getHeight() - bgSize)) / 2;

                Log.i("SetPrefs", "Background size is " + bgSize);
                Bitmap cropped = Bitmap.createBitmap(source, (int) offsetX, (int) offsetY, bgSize, bgSize);

                createCroppedImage(cropped);

            } else if (requestCode == CROP_FROM_CAMERA) {
                Bundle extras = data.getExtras();

                if (extras != null) {
                    Bitmap cropped = extras.getParcelable("data");
                    createCroppedImage(cropped);

                } else {
                    bgImageError();
                }

            }

        } else {
            Log.i("SetPrefs", "Could not get a correct background image");
        }
    }

    private static int getMinimumPowerOfTwo(int shortestSize) {
        int power = 2;
        while (shortestSize > power * 2) {
            power *= 2;
        }
        return power;
    }

    private void bgImageError() {
        displayError("Error cropping image", "Error cropping image. Remember the image must be at least " + bgSize + "x" + bgSize + ".");
    }

    private void displayError(String title, String text) {
        //Display error message
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(text);

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void createCroppedImage(Bitmap cropped) {
        try {
            boolean mExternalStorageAvailable = false;
            boolean mExternalStorageWriteable = false;
            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
            } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
            } else {
                // Something else is wrong. It may be one of many other states, but all we need
                //  to know is we can neither read nor write
                mExternalStorageAvailable = mExternalStorageWriteable = false;
            }

            if (mExternalStorageAvailable && mExternalStorageWriteable) {
                File dir = createDirIfNotExists(NBODY_DIR);
                OutputStream fOut = null;
                File file = new File(dir, "NBodyBG.jpg");
                if (file.exists())
                    file.delete();

                fOut = new FileOutputStream(file);

                cropped.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.flush();
                fOut.close();

                //MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

                //Finally, update preferences
                this.getPreferenceScreen().getEditor().putString(Constants.PREF_BGIMAGE, file.getAbsolutePath()).commit();
                this.getPreferenceScreen().getEditor().putString(KEY_BG_IMG_PATH, mLastBgImgPath).commit();
                changes++;
                updateBackgroundPreferencesState();
            } else {
                displayError("External storage error", "Extearnal storage is not available or not writable!");
            }

        } catch (Exception e) {
            Log.e("SetPrefs", e + ": " + e.getMessage());
        } finally {

        }
    }

    private void updateBackgroundPreferencesState() {
        Preference col = getPreferenceScreen().findPreference(Constants.PREF_BGCOLOR);
        Preference img = getPreferenceScreen().findPreference(Constants.PREF_BGIMAGE);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        String bgimg = sp.getString(Constants.PREF_BGIMAGE, "");
        String lastbgimg = sp.getString(KEY_BG_IMG_PATH, null);
        int bgcolor = sp.getInt(Constants.PREF_BGCOLOR, Color.BLACK);

        if (bgimg == null || bgimg.length() == 0) {
            //No background image!
            col.setSummary("Color currently selected: " + bgcolor);
            img.setSummary("No background image selected");
        } else {
            //Background image!
            col.setSummary("No color selected");
            img.setSummary("Background image selected: " + (lastbgimg == null ? bgimg : lastbgimg));
        }
    }

    public String getPath(Uri uri) {
        String selectedImagePath;
        //1:MEDIA GALLERY --- query from MediaStore.Images.Media.DATA
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            selectedImagePath = cursor.getString(column_index);
        } else {
            selectedImagePath = null;
        }

        if (selectedImagePath == null) {
            //2:OI FILE Manager --- call method: uri.getPath()
            selectedImagePath = uri.getPath();
        }
        return selectedImagePath;
    }

    public static File createDirIfNotExists(String path) {

        File file = new File(Environment.getExternalStorageDirectory(), path);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.e("SetPrefs", "Problem creating Image folder");
                return null;
            }
        }
        return file;
    }

}
